
Drupal.behaviors.itworld = function(context) {

  $('.form-item .description').each(function(){
      help_box = '<span class="help-text"></span>';
      $(this).parent('.form-item').find('label').append(help_box);
      $(this).hide();
  });
  $('body').each(function(){
    var hideDelayTimer = null;
    // tracker
    var shown = false;

  $('.help-text').mouseover(function(event){
      if (hideDelayTimer) clearTimeout(hideDelayTimer);

      
      //id of item been shown
      var id = $(this).parent().parent('.form-item').attr('id');
    
      // don't trigger the animation again if we're being shown, or already visible
      if (shown) {
        return;
      } else {
          help_text = $(this).parent().parent('.form-item').find('.description').text();
          help_box =  '<div class="pop-description">'+ help_text +'</div>';
      
          left = event.pageX + 25 +'px';
          top =  event.pageY - 10 +'px';
          shown = true;
    
          $(this).append(help_box);
          $('#'+ id +' .pop-description').appendTo('body').css({'position':'absolute','top':top, 'left':left}).mouseover(function(){
              $(this).addClass('hold-on');
          }).mouseout(function(){
              $(this).removeClass('hold-on');
              if (hideDelayTimer) clearTimeout(hideDelayTimer);
     
              hideDelaytimer = setTimeout(function(){
              if(!$('.pop-description').hasClass('hold-on')){
                  $('.pop-description').hide();
                  shown = false;
                  }
              }, 500);              
            });
          
      }
  }).mouseout(function(){
      if (hideDelayTimer) clearTimeout(hideDelayTimer);

      
      hideDelaytimer = setTimeout(function(){
        if(!$('.pop-description').hasClass('hold-on')){
            $('.pop-description').hide();
            shown = false;
        }
      }, 500);

  });
 });  
}
