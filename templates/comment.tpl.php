<?php
?>

<div class="comment<?php print $comment_by_author; ?>">

  <div class="comment-info"> 

    <div class="submitted">
        <div class="comment-name"><?php print theme('username', $comment) ?></div>
        <spam class="date"><?php print $date ?></spam>
        <?php if ($comment->new) : ?>
          <a id="new"></a>
          <span class="new"><?php print $new ?></span>
        <?php endif; ?>        
    </div>

    <?php if ($permalink) : ?>
      <span class="comment-permalink"><?php print $permalink ?></span>
    <?php endif; ?>
  </div>

  <div class="comment-content">

    <div class="content">
      <?php print $picture ?>    
      <?php print $content ?>
    </div>
    <?php print $links ?>


  </div>
  <div class="clear"></div>
</div>

