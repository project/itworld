<?php

// Include the definition of opengrid_settings, opengrid_initialize_theme_settings() and opengrid_default_theme_settings().
include_once './' . drupal_get_path('theme', 'opengrid') . '/theme-settings.php';


/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function itworld_settings($saved_settings) {

  global $base_url;
  
  //print dsm($saved_settings);

  // Get theme name from url (admin/.../theme_name)
  $theme_name = arg(count(arg()) - 1);

  // Combine default theme settings from .info file & theme-settings.php
  $theme_data = list_themes();   // get data for all themes
  $info_theme_settings = ($theme_name) ? $theme_data[$theme_name]->info['settings'] : array();
  $defaults = array_merge(opengrid_default_theme_settings(), $info_theme_settings);
  
  // Combine default and saved theme settings
  $settings = array_merge($defaults, $saved_settings);
  
  // Check for a new uploaded logo, and use that instead.
  $directory_path = file_directory_path();
  file_check_directory($directory_path, FILE_CREATE_DIRECTORY, 'file_directory_path');
  
  if ($file = file_save_upload('sitename_upload', array('file_validate_is_image' => array()))) {
    $parts = pathinfo($file->filename);
    $filename = str_replace('/', '_', $theme_name) . '_sitename.' . $parts['extension'];

    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
      $_POST['sitename_path'] = $file->filepath;
    }
  }
  
  
  // Create the form using Forms API: http://api.drupal.org/api/6
  $form = array();
  
  $form['copyright'] = array(
    '#type' => 'fieldset',
    '#title' => t('Copyright info'),
    '#collapsible' => true,
    '#description' => t('Shows copyright info in the footer region.'),
  );
  $form['copyright']['copyright_switch'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show copyrighth info'),
    '#default_value' => $settings['copyright_switch'],
  );  
  $form['copyright']['start'] = array(
    '#type' => 'textfield',
    '#title' => t('Year of start'),   
    '#default_value' => $settings['start'],
    '#size' => 10,
  ); 
  $form['header_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header Message'),
    '#description' => t('Message to show in the header.'),
  );
  $form['header_message']['header_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Messsage'),   
    '#default_value' => $settings['header_text'],
  );
    $form['social'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Links'),
    '#collapsible' => true,
    '#collpased' => false,
  );
  $form['social']['social_box'] = array(
    '#type' => 'checkbox',
    '#title' => t('Social Box'),
    '#description' => t('Enables Social Box in theme header'),
    '#default_value' => $settings['social_box'],    
  );
  $form['social']['social_links'] = array(
    '#type' => 'fieldset',
    '#tree' => true,
  );
  $form['social']['social_links']['youtube'] = array(
    '#type' => 'textfield',
    '#title' => t('youtube'),
    '#default_value' => $settings['social_links']['youtube'],
    '#size' => 60,
    '#attributes' => array('class' => 'social youtube'),
  );
  $form['social']['social_links']['linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('linkedin'), 
    '#default_value' => $settings['social_links']['linkedin'],
    '#size' => 60,
    '#attributes' => array('class' => 'social linkedin'),
  );  
  $form['social']['social_links']['facebook'] = array(
    '#type' => 'textfield',
    '#title' => t('facebook'), 
    '#default_value' => $settings['social_links']['facebook'],
    '#size' => 60,
    '#attributes' => array('class' => 'social facebook'),
  ); 
  $form['social']['social_links']['flickr'] = array(
    '#type' => 'textfield',
    '#title' => t('flickr'), 
    '#default_value' => $settings['social_links']['flickr'],
    '#size' => 60,
    '#attributes' => array('class' => 'social flickr'),
  ); 
  $form['social']['social_links']['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('twitter'), 
    '#default_value' => $settings['social_links']['twitter'],
    '#size' => 60,
    '#attributes' => array('class' => 'social twitter'),
  );

  // Add the base theme's settings.
  $form += opengrid_settings($saved_settings, $defaults);

  return $form;
}
