<?php

function itworld_preprocess_page(&$vars){

  $copyright = $title = theme_get_setting('copyright_switch');
  if($copyright){
    $vars['copyright'] = $vars['site_name'] .' '. check_plain(theme_get_setting('start')) .' - '. t('Now');  
  }
  $vars['home'] = t('Home');
  //removes sidebars for admin pages and node forms
  if(arg(0) == 'admin' || (arg(0) == 'node' && arg(2) == 'edit') || (arg(0) == 'node' && arg(1) == 'add')){
    $vars['sidebar_first'] = NULL;
    $vars['sidebar_second'] = NULL; 
    $container_value = theme_get_setting('container_columns');
    $container_value = str_replace('container','grid',$container_value);
    $vars['main_grid'] =  $container_value;
  }
  //Ads a variable to body when theme search box is active
  if($vars['search_box']){
    $body_classes = explode(' ',$vars['body_classes']);
    $body_classes[] = 'search-box';
    $vars['body_classes'] = implode(' ', $body_classes); 
  }
  
  //Header message variable
  $header_message = theme_get_setting('header_text');
  if($header_message){
    $vars['header_message'] = filter_filter('process', 1, -1, $header_message);  
  }
  //Social
  if(theme_get_setting('social_box') || module_exists('aggregator')){
    $social_links = theme_get_setting('social_links');
    foreach($social_links as $key => $value){
      if($value != '' && theme_get_setting('social_box')){
        $key_class = str_replace(' ','-', $key);
        $img_name = 'icon_'. $key .'.png';
        $img = '<img title ="'. $key .'" alt="'. $key .'" src="/'. path_to_theme() .'/images/social/'. $img_name .'" />';
        $social[$key] = l($img, $value, array('attributes' => array('class' => 'social-icon '. $key_class), 'html' => true));
      }  
    }
    if(module_exists('aggregator') ){
      $social['feed'] = $vars['feed_icons'];
    }
    $vars['social'] = theme('item_list', $social);    
  }
}

/**
* Implementation of hook_theme().
*/
function itworld_theme(){
  return array(
    'comment_form' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}
/**
* Theme the output of the comment_form.
*
* @param $form
*   The form that  is to be themed.
*/
function itworld_comment_form($form) {
;
  $form['_author']['#prefix'] = '<div class="author-name">'; 
  $form['_author']['#suffix'] = '</div>';    
  $form['comment_filter']['format']['#type'] = 'hidden';
  $form['comment_filter']['comment']['#title'] = '';
  $form['comment_filter']['comment']['#rows'] = 7;  
  
  //notify override
  if(module_exists('comment_notify')){
    $form['notify_settings']['#prefix'] = '<fieldset class="collapsed collapsible"><legend>'. t('Notify me') .'</legend><div class="clear-block notify-box">';
    $form['notify_settings']['#suffix'] = '</div></fieldset>';
  }
 
  return drupal_render($form);
}

function itworld_preprocess_comment_wrapper($vars){
  $node = node_load(arg(1));
  $vars['comment_count'] = $node->comment_count != 0 ? format_plural($node->comment_count, t('1 Comment'), t('@count Comments')) : t('No comments');
  $vars['no_comments_message'] = $node->comment_count == 0 ? t('Be the first to post a comment') : '';
}

/********************************
 * Hook to preprocess comment
*********************************/
function itworld_preprocess_comment(&$vars){
  //adding variable for permalink
    $link['href'] = 'node/'. $vars['node']->nid;
    $link['fragment'] = 'comment-'. $vars['comment']->cid;
    //$link['title'] = filter_xss(t('Permalink'), array());
    $link['title'] = $vars['id'];
    $link['attributes']['title'] = $vars['comment']->title;
    // Force an absolute link.
    $link['absolute'] = TRUE;
    // Stop Drupal from looking up the alias to the path.
    $link['alias'] = TRUE;
    // We do our own filtering, so prevent double filtering.
    $link['html'] = TRUE; 
    // Add rel="nofollow" to link if the option is enabled.    
    $link['attributes']['rel'] = 'nofollow';
    
    $vars['permalink'] = l($link['title'], $link['href'], $link);  
    //comment by author flag
    $vars['comment_by_author'] = ($vars['comment']->uid == $vars['node']->uid) ? ' comment-by-author' : '';
} 
