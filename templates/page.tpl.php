<?php
?>
<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
    <!--[if IE 7]>
      <?php print $ie7_styles; ?>
    <![endif]-->
    <!--[if lte IE 6]>
      <?php print $ie6_styles; ?>
    <![endif]-->  
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>

<body class="<?php print $body_classes; ?>">

  <div id="page" class="<?php print $page_classes; ?>">
  <div class="inner-page">
  
    <div class="header-wrapper">
        <div id="container-header" class="<?php print $header_classes; ?>">
            <div id="header" class="region <?php print $header_grid; ?>">
                <?php if($search_box): ?>
                    <div class="search-box"><?php print $search_box; ?></div>
                <?php endif; ?>   
                <?php if($logo): ?>
                    <a class="logo-img" title="<?php print $home; ?>" href="<?php print $base_path; ?>">
                        <img title="<?php print $home; ?>" alt="<?php print $site_name; ?>" src="<?php print $logo; ?>" />
                    </a>
                <?php endif; ?>
                <?php if($site_name || $site_slogan): ?>
                    <div class="site-name">
                        <a title="<?php print $home; ?>" href="<?php print check_url($front_page) ?>">
                            <?php print $site_name; ?>
                            <?php if($sitename_path): ?>
                                <img title="<?php print $sitename_title; ?>" alt="<?php print $sitename_alt; ?>" src="<?php print $sitename_path; ?>" />
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="site-slogan"><?php print $site_slogan;?></div>
                <?php endif;?>
                <?php if($header): ?>
                    <div class="header-region">
                        <?php print $header; ?>
                    </div>
                <?php endif; ?>  
                <?php if($header_message): ?>
                  <div class="header-message">
                    <?php print $header_message; ?>
                  </div>
                <?php endif; ?> 
                <?php if(!empty($social)): ?>
                  <div id="social"><?php print $social; ?></div>
                <?php endif; ?>                             
            </div>
        </div>
    </div>
    
        <?php //print $grid_control; ?>  

    <?php if($breadcrumb || $content_top): ?>    
      <div class="content-top-wrapper">
	<div id="container-content-top" class="<?php print $content_top_classes; ?>">
	  <div id="content-top" class="region <?php print $content_top_grid; ?>">
	      <?php if($breadcrumb): ?>
		<div id="breadcrumbs">
		    <?php print $breadcrumb; ?>
		</div>
	      <?php endif; ?>	
	      <?php print $content_top; ?>
	  </div>     
	</div>
      </div>
    <?php endif;?>      
    
    <div class="content-wrapper"><div id="container-content" class="<?php print $content_classes; ?>">
	<div id="content" class="region <?php print $content_grid; ?>">

	  <div id="main" class="region <?php print $main_grid; ?>">
             <?php if($mission): ?>
                <div id="mission">
		    <?php print $mission; ?>
                </div>
             <?php endif; ?><!-- #mission -->  

	     <?php if (!empty($tabs)): print '<div id="tabs-wrapper" >'; endif; ?>
             <?php if ($title): ?><h1 class="title <?php print $title_class; ?>" id="page-title" ><?php print $title; ?></h1><?php endif; ?>
             <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div></div><?php endif; ?>
	     <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
             <?php if ($messages): print $messages; endif; ?>
             <?php if ($help): print $help; endif; ?>
	
	    <div class="content-output">
		<?php print $content; ?>
	    </div>
      
	  </div><!-- #main ends here --> 
	  
	  <?php if($sidebar_first): ?>
	      <div id="sidebar-first" class="region <?php print $sidebar_first_grid; ?>">
            <?php print $sidebar_first; ?>
	      </div>
	  <?php endif; ?><!-- #first ends here -->

	  <?php if($sidebar_second): ?>
        <div id="sidebar-second" class="region <?php print $sidebar_second_grid; ?>">
            <?php print $sidebar_second; ?>
	      </div>
	  <?php endif; ?> <!-- #second ends here -->

	</div>    
    </div></div><!-- #content ends here --> 

    <?php if($content_bottom): ?>    
      <div class="content-bottom-wrapper">
        <div id="container-content-bottom" class="<?php print $content_bottom_classes; ?>">
          <div id="content-bottom" class="region <?php print $content_bottom_grid; ?>">
            <?php print $content_bottom; ?>
          </div> 
        </div>
      </div> 
    <?php endif;?> 
    
    <?php if($primary_links): ?>
        <div id="main-navigation-wrapper">
            <div id="main-navigation" class="<?php print $main_navigation_classes?>">
                  <div class="primary-links"><?php print $primary_links; ?></div>
            </div>
        </div>
    <?php endif;?> 
    <?php if($secondary_links): ?>
        <div id="secondary-navigation-wrapper">
            <div id="secondary-navigation" class="<?php print $main_navigation_classes?>">
                  <div class="secondary-links"><?php print $secondary_links; ?></div> 
            </div>
        </div>
    <?php endif;?>    
                     
    <?php if($scroll_to_top): ?>
      <div class="scroll-to-top"><a href="#header"><?php print $scroll_to_top; ?></a></div>
    <?php endif; ?>
    
  
    <div class="footer-wrapper"><div id="container-footer" class="<?php print $footer_classes; ?>">
	<div id="footer" class="region <?php print $footer_grid; ?>">
	    <?php if($footer): ?>
		<div id="footer-region"><?php print $footer; ?></div>
	    <?php endif; ?>
	    
	    <?php if($footer_message): ?>
        <div class="footer-message"><?php print $footer_message; ?></div>
	    <?php endif; ?>
	    <?php if(!empty($copyright)): ?>
        <div class="copyright"><?php print $copyright; ?></div>
      <?php endif; ?> 
      <?php print $feed_icons ?>       
	</div>    
    </div></div>
  </div><!-- .inner-page ends here --> 
    <?php if($tray_region): ?>
        <div class="tray-close">Tray</div>    
        <div id="tray-wrapper">
            <?php print $tray_region; ?>
        </div>
    <?php endif; ?>    
    <?php print $closure; ?>

  </div><!-- #page ends here -->
  <?php print $grid_ui; ?>
</body>
</html>
